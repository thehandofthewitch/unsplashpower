import React, { Component } from 'react';
import Header           from './components/Header';
import Photos         from './components/Photos';
import MainPhoto      from './components/MainPhoto';
import Footer           from './components/Footer';

import { Switch, Route, Redirect } from 'react-router-dom';


import './App.css';

class App extends Component {

    render() {
        return (
        <div className="App container">

            <Header />

                <Switch>

                    <Route path='/photos/' component={Photos}></Route>

                    <Route path='/photo/:id' component={MainPhoto}/>

                    <Redirect from="/" to="/photos"/>
                    
                </Switch>

            <Footer />
        </div>
        );
    }
}

export default App;