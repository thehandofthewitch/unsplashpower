
const initState = {photos:[]};


export default function app(state = initState, action)
{
    switch(action.type)
    {
        case 'SET_PHOTOS':

        return {
                ...state,
                photos: [...action.data]
            };

        default: return state;     
    }
}