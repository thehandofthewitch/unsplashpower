import React, { Component } from 'react';
import Photo     from './Photo';
import {getPhotosAction} from '../actions/app';
import {connect}   from  'react-redux';

class Photos extends Component {

    constructor(params)
    {
        super(params);

        this.state = 
        {
            str:''
        }
    }

    componentDidMount()
    {
        this.props.getPhotos();
    }

    showPhotos()
    {
        let filtered = [];

        for(let j in this.props.photos)
        {
            let photo = this.props.photos[j];

            if(this.state.str)
            {
                if(JSON.stringify(photo).toLowerCase().search(this.state.str.toLowerCase()) !== -1)
                {
                    filtered.push(<Photo key={j} pkey={j} data={photo}  />);
                }
                else{
                    continue;
                }
            }
            else{
                filtered.push(<Photo key={j} pkey={j} data={photo}  />);
            }
        }

       return filtered;
    }

    filterPhotos = (el) => {
        this.setState({str:el.target.value});
    }
    
    render() {
        
        return (
            <div className="photos row page">
                <h3 className="col-md-12" >Photos</h3>
                
                {this.showPhotos()}
            </div>
        );
    }
}

export default connect(
    state => ({
        photos: state.app.photos
    }),
    dispatch => ({
        getPhotos:() => {            
            dispatch(getPhotosAction());
        }
    })
)(Photos);
