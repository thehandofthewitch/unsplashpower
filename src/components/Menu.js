import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
const menu = require('../system/menu');


class Menu extends Component {


  showMenu()
  {
      return menu.map(function(el, k){
          return <li key={k} ><Link to={el.link}>{el.name}</Link></li>
      });
  }


  render() {
      return (
        <nav className="navbar navbar-expand-lg navbar-warning bg-warning">
          <ul className="menu navbar-nav justify-content-around w-100">
              {this.showMenu()}
                
          </ul>
        </nav>
      );
  }
}

export default connect(
    state => ({
       
    }),
    dispatch => ({
       
    })
)(Menu);
