import React, { Component } from 'react';
import {connect}    from 'react-redux';
import { Link } from 'react-router-dom';

class Photo extends Component {

  render() {

      return (
        <div className="col-sm-3">
            <div className="photo text-center">
                <div className="image">
                    <Link to={"/photo/" + this.props.data.id} >
                        <img className="img-fluid" src={this.props.data.urls.small} />
                    </Link>    
                </div>
                <div className="manufacturer">
                    {this.props.data.user.name}
                </div>
                <div className="description">
                    {this.props.data.user.location}
                </div>
            </div>
        </div>
      );
  }


}


export default connect(
    state => ({
       photos:state.app.photos
    }),
    dispatch => ({

    })
)(Photo);
