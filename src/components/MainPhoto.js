import React, { Component } from 'react';
import {connect}    from 'react-redux';
import {getPhotosAction} from '../actions/app';

class MainPhoto extends Component {

    constructor(props)
    {
        super(props);

        this.state = {
            photo:{urls:{full:''}},
            id:this.props.match.params.id
        }
    }

    componentDidMount()
    {   
       
        this.setPhoto();
    }

    setPhoto = ()=>
    {
        let id = this.state.id;
       
        for(let j in this.props.photos)
        {
            if(this.props.photos[j]['id'] === id)
            {
                console.log(this.props.photos[j]);
                this.setState({photo:this.props.photos[j]});
            }
        }
    }

    render() {
        
        return (
            <div className="product row mt-3 mb-3">
                <div className="image col-12">
                    <img className="img-fluid" src={this.state.photo.urls.full} />  
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
       photos:state.app.photos
    }),
    dispatch => ({
        getPhotos:(callback) => {            
            dispatch(getPhotosAction(callback));
        },
        
    })
)(MainPhoto)