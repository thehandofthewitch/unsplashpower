export const getPhotosAction = (callback) => dispatch => 
{   
    fetch("https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0", {
        method:'GET'
    }).then((response)=>
    {
        return response.json()
    }).then((d)=>
    {
        dispatch({type:'SET_PHOTOS', data:d});
        callback ? callback() : '';
    });
}

